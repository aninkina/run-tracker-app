package com.androiddevs.runtrackerapp.adapters.callbacks

import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable

import androidx.recyclerview.widget.RecyclerView

import androidx.recyclerview.widget.ItemTouchHelper


abstract class SwipeToDeleteCallback(private val icon: Drawable?, private val background: ColorDrawable) : ItemTouchHelper.Callback() {



    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val swipeFlag = ItemTouchHelper.LEFT
        return makeMovementFlags(0, swipeFlag)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }
    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        // Check when item is added back to an empty list
        if (viewHolder.adapterPosition == -1) return

        val itemView = viewHolder.itemView

        // Swipe left
        if (dX < 0) {
            // Calculate position of icon
            if (icon != null) {
                val iconMargin = icon.intrinsicHeight
                val iconTop = itemView.top + (itemView.height - icon.intrinsicHeight) / 2
                val iconBottom = iconTop + icon.intrinsicHeight
                val iconLeft = itemView.right - iconMargin - icon.intrinsicWidth
                val iconRight = itemView.right - iconMargin
                // Set bounds for icon to where to draw
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
            }
            // Set bounds for background to where to draw
            background.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
        }
        // Swipe right
        else if (dX > 0) {
            // Calculate position of icon
            if (icon != null) {
                val iconMargin = icon.intrinsicHeight
                val iconTop = itemView.top + (itemView.height - icon.intrinsicHeight) / 2
                val iconBottom = iconTop + icon.intrinsicHeight
                val iconLeft = itemView.left + iconMargin
                val iconRight = itemView.left + iconMargin + icon.intrinsicWidth
                // Set bounds for icon to where to draw
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
            }
            // Set bounds for background  to where to draw
            background.setBounds(itemView.left, itemView.top, itemView.left + dX.toInt(), itemView.bottom)
        }
        else {
            background.setBounds(0, 0, 0,0)
            icon?.setBounds(0, 0, 0, 0)
        }

        // Draw
        background.draw(c)
        icon?.draw(c)
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}
